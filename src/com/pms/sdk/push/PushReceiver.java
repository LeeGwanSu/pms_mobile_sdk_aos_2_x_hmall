package com.pms.sdk.push;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationChannelGroup;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.service.notification.StatusBarNotification;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;
import android.widget.Toast;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.android.volley.toolbox.ImageLoader.ImageListener;
import com.android.volley.toolbox.Volley;
import com.pms.sdk.IPMSConsts;
import com.pms.sdk.PMS;
import com.pms.sdk.api.QueueManager;
import com.pms.sdk.bean.Msg;
import com.pms.sdk.bean.PushMsg;
import com.pms.sdk.common.util.*;
import com.pms.sdk.db.PMSDB;
import com.pms.sdk.push.mqtt.RestartReceiver;
import com.pms.sdk.view.BitmapLruCache;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

import static android.content.Context.VIBRATOR_SERVICE;

/**
 * push receiver
 *
 * @author erzisk
 * @since 2013.06.07
 */
public class PushReceiver extends BroadcastReceiver implements IPMSConsts {

    // notification id
    public final static int NOTIFICATION_ID = 0x253470;
    private static final int START_TASK_TO_FRONT = 2;
    private static final String USE = "Y";
    private static final int DEFAULT_SHOWING_TIME = 30000;
    private static final int NOTIFICATION_GROUP_SUMMARY_ID = 1;
    private static final String NOTIFICATION_GROUP = "com.apms.sdk.notification_type";

    private static AtomicInteger sNotificationId = new AtomicInteger(NOTIFICATION_GROUP_SUMMARY_ID);

    private final Handler mFinishHandler = new Handler();
    private Prefs mPrefs;
    private PowerManager pm;
    private PowerManager.WakeLock wl;
    /**
     * finish runnable
     */
    private final Runnable finishRunnable = new Runnable() {
        @Override
        public void run() {
            if (wl != null && wl.isHeld()) {
                wl.release();
            }
        }

        ;
    };
    private boolean mbPushImage = false;
    private Bitmap mPushImage;

    /**
     * gcm key register
     *
     * @param context
     * @param senderId
     */
    public static void gcmRegister(Context context, String senderId) {
        try
        {
            Intent registrationIntent = new Intent(ACTION_REGISTER);
            registrationIntent.setPackage(GSF_PACKAGE);
            registrationIntent.putExtra(KEY_APP, PendingIntent.getBroadcast(context, 0, new Intent(), 0));
            registrationIntent.putExtra(KEY_SENDER, senderId);
            context.startService(registrationIntent);

        }
        catch (Exception e)
        {
            CLog.e(e.getMessage());
        }
    }

    public static int getNotificationId() {
        return sNotificationId.get();
    }

    @Override
    public synchronized void onReceive(final Context context, final Intent intent) {
        CLog.i("onReceive() -> " + intent.toString());
        mPrefs = new Prefs(context);

        if (intent.getAction().equals(ACTION_REGISTRATION)) {
            // registration
            CLog.i("onReceive:registration");
            if (intent.getStringExtra(KEY_GCM_TOKEN) != null) {
                // regist gcm key
                CLog.d("handleRegistration:key=" + intent.getStringExtra(KEY_GCM_TOKEN));
                mPrefs.putString(KEY_GCM_TOKEN, intent.getStringExtra(KEY_GCM_TOKEN));
            } else {
                // error occurred
                CLog.i("handleRegistration:error=" + intent.getStringExtra("error"));
                CLog.i("handleRegistration:unregistered=" + intent.getStringExtra("unregistered"));
            }
        } else {
            // receive push message
//            if (intent.getAction().equals(MQTTService.INTENT_RECEIVED_MSG)) {
//                // private server
//                String message = intent.getStringExtra(MQTTService.KEY_MSG);
//
//                CLog.i("onReceive:receive from private server");
//
//                // set push info
//                try {
//                    JSONObject msgObj = new JSONObject(message);
//                    if (msgObj.has(KEY_MSG_ID)) {
//                        intent.putExtra(KEY_MSG_ID, msgObj.getString(KEY_MSG_ID));
//                    }
//                    if (msgObj.has(KEY_NOTI_TITLE)) {
//                        intent.putExtra(KEY_NOTI_TITLE, msgObj.getString(KEY_NOTI_TITLE));
//                    }
//                    if (msgObj.has(KEY_MSG_TYPE)) {
//                        intent.putExtra(KEY_MSG_TYPE, msgObj.getString(KEY_MSG_TYPE));
//                    }
//                    if (msgObj.has(KEY_NOTI_MSG)) {
//                        intent.putExtra(KEY_NOTI_MSG, msgObj.getString(KEY_NOTI_MSG));
//                    }
//                    if (msgObj.has(KEY_MSG)) {
//                        intent.putExtra(KEY_MSG, msgObj.getString(KEY_MSG));
//                    }
//                    if (msgObj.has(KEY_SOUND)) {
//                        intent.putExtra(KEY_SOUND, msgObj.getString(KEY_SOUND));
//                    }
//                    if (msgObj.has(KEY_NOTI_IMG)) {
//                        intent.putExtra(KEY_NOTI_IMG, msgObj.getString(KEY_NOTI_IMG));
//                    }
//                    if (msgObj.has(KEY_COLOR_FLAG)) {
//                        intent.putExtra(KEY_COLOR_FLAG, msgObj.getString(KEY_COLOR_FLAG));
//                    }
//                    if (msgObj.has(KEY_TITLE_COLOR)) {
//                        intent.putExtra(KEY_TITLE_COLOR, msgObj.getString(KEY_TITLE_COLOR));
//                    }
//                    if (msgObj.has(KEY_CONTENT_COLOR)) {
//                        intent.putExtra(KEY_CONTENT_COLOR, msgObj.getString(KEY_CONTENT_COLOR));
//                    }
//                    if (msgObj.has(KEY_NOT_POPUP)) {
//                        intent.putExtra(KEY_NOT_POPUP, msgObj.getString(KEY_NOT_POPUP));
//                    }
//                    if (msgObj.has(KEY_DATA)) {
//                        intent.putExtra(KEY_DATA, msgObj.getString(KEY_DATA));
//                    }
//                } catch (Exception e) {
//                    CLog.e(e.getMessage());
//                }
//                onPushMessage(context, intent);
//            } else


            if (intent.getAction().equals(ACTION_RECEIVE)) {
                CLog.i("onReceive:receive from GCM");
                onPushMessage(context, intent);
            }
        }
    }

    private synchronized void onPushMessage(final Context context, final Intent intent) {
        String firstTime = PMSUtil.getFirstTime(context);
        String secondTime = PMSUtil.getSecondTime(context);
        Boolean pushCancel = true;

        CLog.i("Allowed Time Flag : " + PMSUtil.getAllowedTime(context));

        if (PMSUtil.getAllowedTime(context)) {
            if ((StringUtil.isEmpty(firstTime) == false) && (StringUtil.isEmpty(secondTime) == false)) {
                pushCancel = DateUtil.getCompareDate(firstTime, secondTime);
            }
        }

        CLog.i("Auto Push Cancel : " + pushCancel);
        if (pushCancel) {
            if (isImagePush(intent.getExtras())) {
                try {
                    // image push
//                    RequestQueue queue = Volley.newRequestQueue(context);
                    QueueManager queueManager = QueueManager.getInstance();
                    RequestQueue queue = queueManager.getRequestQueue();
                    queue.getCache().clear();
                    ImageLoader imageLoader = new ImageLoader(queue, new BitmapLruCache());
                    imageLoader.get(intent.getStringExtra(KEY_NOTI_IMG), new ImageListener() {
                        @Override
                        public void onResponse(ImageContainer response, boolean isImmediate) {
                            if (response == null) {
                                CLog.e("response is null");
                                return;
                            }
                            if (response.getBitmap() == null) {
                                CLog.e("bitmap is null");
                                return;
                            }

                            mbPushImage = true;
                            mPushImage = response.getBitmap();
                            CLog.i("imageWidth:" + mPushImage.getWidth());
                            onMessage(context, intent);
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            CLog.e("onErrorResponse:" + error.getMessage());
                            mbPushImage = false;
                            // wrong img url (or exception)
                            onMessage(context, intent);
                        }
                    });
                } catch (Exception e) {
                    CLog.e(e.getMessage());
                    onMessage(context, intent);
                }
            } else {
                // default push
                onMessage(context, intent);
            }
        }
    }

    /**
     * on message (gcm, private msg receiver)
     *
     * @param context
     * @param intent
     */
    private synchronized void onMessage(final Context context, Intent intent) {

        final Bundle extras = intent.getExtras();

        PMS pms = PMS.getInstance(context);

        PushMsg pushMsg = new PushMsg(extras);

        if (StringUtil.isEmpty(pushMsg.msgId) || StringUtil.isEmpty(pushMsg.notiTitle) || StringUtil.isEmpty(pushMsg.notiMsg)
                || StringUtil.isEmpty(pushMsg.msgType)) {
            CLog.i("msgId or notiTitle or notiMsg or msgType is null");
            if (FLAG_Y.equals(PMSUtil.getMQTTFlag(context)) && FLAG_Y.equals(PMSUtil.getPrivateFlag(context)))
            {
                CLog.i("msgId or notiTitle or notiMsg or msgType is null");
                if (FLAG_Y.equals(mPrefs.getString(IPMSConsts.PREF_SCREEN_WAKEUP_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_SCREEN_WAKEUP_FLAG))) {
                    // screen on
                    pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
                    wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "myapp:mywakelocktag");
                    if (!pm.isScreenOn()) {
                        wl.acquire();
                        mFinishHandler.postDelayed(finishRunnable, DEFAULT_SHOWING_TIME);
                    }
                }
                Intent sendIntent = new Intent(context, RestartReceiver.class);
                sendIntent.setAction(ACTION_FORCE_START);
                context.sendBroadcast(sendIntent);
            }
            return;
        }

        CLog.i(pushMsg + "");

        PMSDB db = PMSDB.getInstance(context);

        // check already exist msg
        Msg existMsg = db.selectMsgWhereMsgId(pushMsg.msgId);
        if (existMsg != null && existMsg.msgId.equals(pushMsg.msgId)) {
            CLog.i("already exist msg");
            return;
        }
        else
        {
            // insert (temp) new msg
            Msg newMsg = new Msg();
            newMsg.readYn = Msg.READ_N;
            newMsg.msgGrpCd = "999999";
            newMsg.expireDate = "0";
            newMsg.msgId = pushMsg.msgId;

            db.insertMsg(newMsg);
        }
        // refresh list and badge
        Intent intentPush = null;

        String receiverClass = null;
//        receiverClass = ProPertiesFileUtil.getString(context, PRO_RECEIVER_CLASS);
        receiverClass = mPrefs.getString(IPMSConsts.PREF_PUSH_RECEIVER_CLASS);
        if (receiverClass != null) {
            try {
                Class<?> cls = Class.forName(receiverClass);
                intentPush = new Intent(context, cls).putExtras(extras);
                intentPush.setAction(RECEIVER_PUSH);
            } catch (ClassNotFoundException e) {
                CLog.e(e.getMessage());
            }
        }
        if (intentPush == null) {
            intentPush = new Intent(RECEIVER_PUSH).putExtras(extras);
        }

        if (intentPush != null) {
            intentPush.addCategory(context.getPackageName());
            context.sendBroadcast(intentPush);
        }

        // show noti
        // ** 수정 ** 기존 코드는 PREF_MSG_FLAG로 되어 있어 NOTI_FLAG로 변경 함

        CLog.i("NOTI FLAG : " + mPrefs.getString(PREF_NOTI_FLAG));
        CLog.i("MKT FLAG : " + mPrefs.getString(PREF_MKT_FLAG));

        if (USE.equals(mPrefs.getString(PREF_NOTI_FLAG)) || //StringUtil.isEmpty(mPrefs.getString(PREF_NOTI_FLAG)) ||
            USE.equals(mPrefs.getString(PREF_MKT_FLAG))) {
            // check push flag
            if (USE.equals(mPrefs.getString(IPMSConsts.PREF_SCREEN_WAKEUP_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_SCREEN_WAKEUP_FLAG))) {
                // screen on
                pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
                wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "myapp:mywakelocktag");
                if (!pm.isScreenOn()) {
                    wl.acquire();
                    mFinishHandler.postDelayed(finishRunnable, DEFAULT_SHOWING_TIME);
                }
            }
            CLog.i("version code :" + Build.VERSION.SDK_INT);

            // execute push noti listener
            if (pms.getOnReceivePushListener() != null) {
                if (pms.getOnReceivePushListener().onReceive(context, extras)) {
                    showNotification(context, extras);
                }
            } else {
                showNotification(context, extras);
            }

            CLog.i("ALERT FLAG : " + mPrefs.getString(PREF_ALERT_FLAG)+" NOT POPUP FLAG : " + pushMsg.notPopup);

            if ((USE.equals(pushMsg.notPopup) == false) && (USE.equals(mPrefs.getString(PREF_ALERT_FLAG)))) {
                showPopup(context, extras);
            }
        }
    }

    /**
     * show notification
     *
     * @param context
     * @param extras
     */
    private synchronized void showNotification(Context context, Bundle extras) {
        CLog.i("showNotification");
        CLog.i("Push Image State ->" + mbPushImage);
        if (mbPushImage) {
            showNotificationImageStyle(context, extras);
        } else {
            showNotificationTextStyle(context, extras);
        }
    }

    /**
     * show notification text style
     *
     * @param context
     * @param extras
     */
    private synchronized void showNotificationTextStyle(Context context, Bundle extras) {
        CLog.i("showNotificationTextStyle");
        int notificationId = getNewNotificationId();
        // push info
        PushMsg pushMsg = new PushMsg(extras);

        // notification
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder;

        String strNotiChannel = DataKeyUtil.getDBKey(context, DB_NOTI_CHANNEL_ID);
        if (StringUtil.isEmpty(strNotiChannel)) {
            strNotiChannel = "0";
        }

        // Notification channel added
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
            strNotiChannel = createNotiChannel(context, notificationManager, strNotiChannel);
            builder = new NotificationCompat.Builder(context, strNotiChannel);
            builder.setNumber(0);
        }
        else {
            builder = new NotificationCompat.Builder(context);
//            builder.setNumber(PMSDB.getInstance(context).selectNewMsgCnt());
        }
        builder.setContentIntent(makePendingIntent(context, extras, notificationId));
        builder.setAutoCancel(true);
        builder.setContentText(pushMsg.notiMsg);
        builder.setContentTitle(pushMsg.notiTitle);
        builder.setTicker(pushMsg.notiMsg);
        // setting lights color
        builder.setLights(Notification.FLAG_SHOW_LIGHTS, 1000, 2000);

        // Android 6.0 ~ 7.0에서 알람 소리를 커스텀 했을 경우 소리가 안나는 현상 대응
        final int noti_mode = ((AudioManager) context.getSystemService(Context.AUDIO_SERVICE)).getRingerMode();

        // 알림 볼륨을 링톤에 적용하여 해당 크기로 울리게 함 ..
        final AudioManager audio = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        final int notiVolume = audio.getStreamVolume(AudioManager.STREAM_NOTIFICATION);
        final int ringVolume = audio.getStreamVolume(AudioManager.STREAM_RING);
        CLog.i("noti => " + notiVolume);

        switch (noti_mode) {
            case AudioManager.RINGER_MODE_NORMAL:
                if (FLAG_Y.equals(mPrefs.getString(PREF_RING_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_RING_FLAG))) {
                    try {
                        if (notiVolume != 0) {
                            audio.setStreamVolume(AudioManager.STREAM_RING, notiVolume == 0 ? 1 : notiVolume, 0);
                            int notiSound = mPrefs.getInt(PREF_NOTI_SOUND);
                            CLog.d("notiSound : "+notiSound);
                            if (notiSound > 0) {
                                Uri soundUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + notiSound);
                                Ringtone ringtone = RingtoneManager.getRingtone(context, soundUri);
                                ringtone.play();
                            } else {
                                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                                Ringtone ringtone = RingtoneManager.getRingtone(context, notification);
                                ringtone.play();
                            }
                            // 링톤 볼륨 원복
                            TimerTask tt = new TimerTask() {
                                @Override
                                public void run() {
                                    audio.setStreamVolume(AudioManager.STREAM_RING, ringVolume, 0);
                                }
                            };
                            Timer t = new Timer();
                            t.schedule(tt, 1000);
                        }
                    } catch (Exception e) {
                        CLog.e(e.getMessage());
                    }
                }
                break;

            case AudioManager.RINGER_MODE_VIBRATE:
                if (FLAG_Y.equals(mPrefs.getString(PREF_VIBE_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_VIBE_FLAG))) {
                    builder.setDefaults(Notification.DEFAULT_VIBRATE);
                }
                break;

            case AudioManager.RINGER_MODE_SILENT:
                break;
        }

        int iconId = 0;
        int largeIconId = 0;
        int ver = Build.VERSION.SDK_INT;
        if (ver >= Build.VERSION_CODES.LOLLIPOP) {
            iconId = PMSUtil.getIconId(context);
            largeIconId = PMSUtil.getLargeIconId(context);
            String strNotiBackColor = PMSUtil.getNotiBackColor(context);
            if (strNotiBackColor != null)
                builder.setColor(Color.parseColor(strNotiBackColor));
        } else {
            iconId = PMSUtil.getLargeIconId(context);
        }

        // set small icon
        CLog.i("small icon :" + iconId);
        if (iconId > 0) {
            builder.setSmallIcon(iconId);
        }

        // set large icon
        CLog.i("large icon :" + largeIconId);
        if (largeIconId > 0) {
            builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), largeIconId));
        }

        int SDK_INT = Build.VERSION.SDK_INT;
        if (PMSUtil.getIsCustNoti(context) && (FLAG_Y.equals(pushMsg.colorFlag)) && (SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)) {
            RemoteViews cutmNoti = new RemoteViews(context.getApplicationContext().getPackageName(), PMSUtil.getNotiResId(context));
            builder.setContent(cutmNoti);
            cutmNoti.setTextViewText(PMSUtil.getNotiTitleResId(context), pushMsg.notiTitle);
            cutmNoti.setTextColor(PMSUtil.getNotiTitleResId(context), Color.parseColor(pushMsg.titleColor));
            cutmNoti.setTextViewText(PMSUtil.getNotiContenResId(context), pushMsg.notiMsg);
            cutmNoti.setTextColor(PMSUtil.getNotiContenResId(context), Color.parseColor(pushMsg.contentColor));
        }

//        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (FLAG_Y.equals(mPrefs.getString(PREF_USE_BIGTEXT))) {
            builder.setStyle(new NotificationCompat.BigTextStyle()
                    .setBigContentTitle(pushMsg.notiTitle).setSummaryText(pushMsg.notiMsg));

        }

        // show notification
        if(ver >= Build.VERSION_CODES.O)
        {
            if(FLAG_Y.equals(mPrefs.getString(PREF_NOTI_GROUP_FLAG)))
            {
                builder.setGroup(NOTIFICATION_GROUP);
            }
            notificationManager.notify(notificationId, builder.build());
        }
        else
        {
            if (ver >= Build.VERSION_CODES.N && FLAG_Y.equals(mPrefs.getString(PREF_NOTI_GROUP_FLAG)))
            {
                builder.setGroup(NOTIFICATION_GROUP);
                updateNotificationSummary(context, notificationManager);
                notificationManager.notify(notificationId, builder.build());
            }
            else
            {
                notificationManager.notify(NOTIFICATION_ID, builder.build());
            }
        }
    }

    /**
     * show notification image style
     *
     * @param context
     * @param extras
     */
    @SuppressLint("NewApi")
    private synchronized void showNotificationImageStyle(Context context, Bundle extras) {
        CLog.i("showNotificationImageStyle");
        int notificationId = getNewNotificationId();
        // push info
        PushMsg pushMsg = new PushMsg(extras);

        // notification
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification.Builder builder;

        String strNotiChannel = DataKeyUtil.getDBKey(context, DB_NOTI_CHANNEL_ID);
        if (StringUtil.isEmpty(strNotiChannel)) {
            strNotiChannel = "0";
        }

        // Notification channel added
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
            strNotiChannel = createNotiChannel(context, notificationManager, strNotiChannel);
            builder = new Notification.Builder(context, strNotiChannel);
            builder.setNumber(0);
        }
        else {
            builder = new Notification.Builder(context);
//            builder.setNumber(PMSDB.getInstance(context).selectNewMsgCnt());
        }
        builder.setContentIntent(makePendingIntent(context, extras, notificationId));
        builder.setAutoCancel(true);
        builder.setContentText(PMSUtil.getBigNotiContextMsg(context));
        builder.setContentTitle(pushMsg.notiTitle);
        builder.setTicker(pushMsg.notiMsg);
        // setting lights color
        builder.setLights(0xFF00FF00, 1000, 2000);

        int iconId = 0;
        int largeIconId = 0;
        int ver = Build.VERSION.SDK_INT;
        if (ver >= Build.VERSION_CODES.LOLLIPOP) {
            iconId = PMSUtil.getIconId(context);
            largeIconId = PMSUtil.getLargeIconId(context);
            String strNotiBackColor = PMSUtil.getNotiBackColor(context);
            if (strNotiBackColor != null)
                builder.setColor(Color.parseColor(strNotiBackColor));
        } else {
            iconId = PMSUtil.getLargeIconId(context);
        }

        // Android 6.0 ~ 7.0에서 알람 소리를 커스텀 했을 경우 소리가 안나는 현상 대응
        final int noti_mode = ((AudioManager) context.getSystemService(Context.AUDIO_SERVICE)).getRingerMode();

        // 알림 볼륨을 링톤에 적용하여 해당 크기로 울리게 함 ..
        final AudioManager audio = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        final int notiVolume = audio.getStreamVolume(AudioManager.STREAM_NOTIFICATION);
        final int ringVolume = audio.getStreamVolume(AudioManager.STREAM_RING);
        CLog.i("noti => " + notiVolume);
        switch (noti_mode) {
            case AudioManager.RINGER_MODE_NORMAL:
                if (FLAG_Y.equals(mPrefs.getString(PREF_RING_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_RING_FLAG))) {
                    try {
                        if (notiVolume != 0) {
                            audio.setStreamVolume(AudioManager.STREAM_RING, notiVolume == 0 ? 1 : notiVolume, 0);
                            int notiSound = mPrefs.getInt(PREF_NOTI_SOUND);
                            CLog.d("notiSound : "+notiSound);
                            if (notiSound > 0) {
                                Uri soundUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + notiSound);
                                Ringtone ringtone = RingtoneManager.getRingtone(context, soundUri);
                                ringtone.play();
                            } else {
                                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                                Ringtone ringtone = RingtoneManager.getRingtone(context, notification);
                                ringtone.play();
                            }
                            // 링톤 볼륨 원복
                            TimerTask tt = new TimerTask() {
                                @Override
                                public void run() {
                                    audio.setStreamVolume(AudioManager.STREAM_RING, ringVolume, 0);
                                }
                            };
                            Timer t = new Timer();
                            t.schedule(tt, 1000);
                        }
                    } catch (Exception e) {
                        CLog.e(e.getMessage());
                    }
                }
                break;

            case AudioManager.RINGER_MODE_VIBRATE:
                if (FLAG_Y.equals(mPrefs.getString(PREF_VIBE_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_VIBE_FLAG))) {
                    builder.setDefaults(Notification.DEFAULT_VIBRATE);
                }
                break;

            case AudioManager.RINGER_MODE_SILENT:
                break;
        }

        // set small icon
        CLog.i("small icon :" + iconId);
        if (iconId > 0) {
            builder.setSmallIcon(iconId);
        }

        // set large icon
        CLog.i("large icon :" + largeIconId);
        if (largeIconId > 0) {
            builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), largeIconId));
        }

        // set large icon
        CLog.i("large icon :" + largeIconId);


        if (largeIconId > 0) {
            Drawable drawable = context.getDrawable(largeIconId);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            builder.setLargeIcon(bitmap);
        }

        if (mPushImage == null) {
            CLog.e("mPushImage is null");
        }

        builder.setStyle(new Notification.BigPictureStyle()
                .bigPicture(mPushImage)
                .setBigContentTitle(pushMsg.notiTitle).setSummaryText(pushMsg.notiMsg));

        // show notification
        if(ver >= Build.VERSION_CODES.O)
        {
            if(FLAG_Y.equals(mPrefs.getString(PREF_NOTI_GROUP_FLAG)))
            {
                builder.setGroup(NOTIFICATION_GROUP);
            }
            notificationManager.notify(notificationId, builder.build());
        }
        else
        {
            if (ver >= Build.VERSION_CODES.N && FLAG_Y.equals(mPrefs.getString(PREF_NOTI_GROUP_FLAG)))
            {
                builder.setGroup(NOTIFICATION_GROUP);
                updateNotificationSummary(context, notificationManager);
                notificationManager.notify(notificationId, builder.build());
            }
            else
            {
                notificationManager.notify(NOTIFICATION_ID, builder.build());
            }
        }

    }

    /**
     * make pending intent
     *
     * @param context
     * @param extras
     * @param requestCode
     * @return
     */
    private PendingIntent makePendingIntent(Context context, Bundle extras, int requestCode) {
        // notification
        Intent innerIntent = null;
//		String receiverClass = ProPertiesFileUtil.getString(context, PRO_NOTI_RECEIVER);
        String receiverClass = mPrefs.getString(PREF_NOTI_RECEIVER_CLASS);
        String receiverAction = mPrefs.getString(PREF_NOTI_RECEIVER);
        CLog.i("makePendingIntent receiverClass : " + receiverClass);
        CLog.i("makePendingIntent receiverAction : " + receiverAction);
        if (receiverClass != null) {
            try {
                Class<?> cls = Class.forName(receiverClass);
                innerIntent = new Intent(context, cls).putExtras(extras);
                if (receiverAction != null)
                    innerIntent.setAction(receiverAction);
            } catch (ClassNotFoundException e) {
                CLog.e(e.getMessage());
            }
        }

        if (innerIntent == null) {
            CLog.d("innerIntent == null");
            receiverAction = receiverAction != null ? receiverAction : "com.pms.sdk.notification";
            // setting push info to intent
            innerIntent = new Intent(receiverAction).putExtras(extras);
        }

        if (innerIntent == null) return null;

//        return PendingIntent.getBroadcast(context, 0, innerIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        int ver = Build.VERSION.SDK_INT;

        if (ver >= Build.VERSION_CODES.N) {
            if (!mPrefs.getString(PREF_NOTI_GROUP_FLAG).isEmpty() &&FLAG_Y.equals(mPrefs.getString(PREF_NOTI_GROUP_FLAG))) {
                return PendingIntent.getBroadcast(context, requestCode, innerIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            } else {
                return PendingIntent.getBroadcast(context, 0, innerIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            }
        } else {
            return PendingIntent.getBroadcast(context, 0, innerIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        }
    }


    /**
     * show popup (activity)
     *
     * @param context
     * @param extras
     */
    @SuppressLint("DefaultLocale")
    private synchronized void showPopup(Context context, Bundle extras) {

        PushMsg pushMsg = new PushMsg(extras);

        if (PMSUtil.getNotiOrPopup(context) && pushMsg.msgType.equals("T")) {
            Toast.makeText(context, pushMsg.notiMsg, Toast.LENGTH_SHORT).show();
        } else {
            Class<?> pushPopupActivity;

            String pushPopupActivityName = mPrefs.getString(PREF_PUSH_POPUP_ACTIVITY);

            if (StringUtil.isEmpty(pushPopupActivityName)) {
                pushPopupActivityName = DEFAULT_PUSH_POPUP_ACTIVITY;
            }

            try {
                pushPopupActivity = Class.forName(pushPopupActivityName);
            } catch (ClassNotFoundException e) {
                CLog.e(e.getMessage());
                pushPopupActivity = PushPopupActivity.class;
            }
            CLog.i("pushPopupActivity :" + pushPopupActivityName);

            Intent pushIntent = new Intent(context, pushPopupActivity);
            pushIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
            pushIntent.putExtras(extras);
            if(isOtherApp(context))
            {
                CLog.i("show popup");
                context.startActivity(pushIntent);
            }
        }
    }

    @SuppressWarnings({"deprecation", "static-access"})
    @SuppressLint("DefaultLocale")
    private boolean isOtherApp(Context context) {

        ActivityManager am = (ActivityManager) context.getSystemService(context.ACTIVITY_SERVICE);
        String topActivity = "";

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            CLog.d("Version is over then KITKAT_WATCH");
            RunningAppProcessInfo currentInfo = null;
            Field field = null;
            try {
                field = RunningAppProcessInfo.class.getDeclaredField("processState");
            } catch (NoSuchFieldException e) {
                CLog.e(e.getMessage());
            }
            List<RunningAppProcessInfo> appList = am.getRunningAppProcesses();
            CLog.d("RunningAppProcessInfo cnt : " + appList.size());

            for (RunningAppProcessInfo app : appList) {
                if (app.importance == RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    Integer state = null;
                    try {
                        state = field.getInt(app);
                        CLog.d("RunningAppProcessInfo getInt : " + state);
                    } catch (IllegalAccessException e) {
                        CLog.e(e.getMessage());
                    } catch (IllegalArgumentException e) {
                        CLog.e(e.getMessage());
                    }
                    if (state != null && state == START_TASK_TO_FRONT) {
                        CLog.d("currentInfo set");
                        currentInfo = app;
//                        break;
                    }
                }
            }
            if (currentInfo != null)
                topActivity = currentInfo.processName;
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(10);
            topActivity = taskInfo.get(0).topActivity.getPackageName();
        }

        CLog.e("TOP Activity : " + topActivity);
        if (topActivity.equals(context.getPackageName())) {
            return true;
        }
        return false;
    }

    /**
     * Adds/updates/removes the notification summary as necessary.
     */
    @TargetApi(Build.VERSION_CODES.M)
    protected void updateNotificationSummary(Context context, NotificationManager manager) {
        final StatusBarNotification[] activeNotifications = manager.getActiveNotifications();

        int numberOfNotifications = activeNotifications.length;
        // Since the notifications might include a summary notification remove it from the count if
        // it is present.
        for (StatusBarNotification notification : activeNotifications) {
            if (notification.getId() == NOTIFICATION_GROUP_SUMMARY_ID) {
                numberOfNotifications--;
                break;
            }
        }

        if (numberOfNotifications > 1) {
            // Add/update the notification summary.
            final NotificationCompat.Builder builder = new NotificationCompat.Builder(context).setStyle(new NotificationCompat.BigTextStyle())
                    .setGroup(NOTIFICATION_GROUP).setGroupSummary(true).setAutoCancel(true);
            int iconId = PMSUtil.getIconId(context);
            // Create a Notification and notify the system.
            if (iconId > 0) {
                builder.setSmallIcon(iconId);
                String strNotiBackColor = PMSUtil.getNotiBackColor(context);
                if (strNotiBackColor != null)
                    builder.setColor(Color.parseColor(strNotiBackColor));
            }

            manager.notify(NOTIFICATION_GROUP_SUMMARY_ID, builder.build());
        } else {
            // Remove the notification summary.
            manager.cancel(NOTIFICATION_GROUP_SUMMARY_ID);
        }
    }

    /**
     * Retrieves a unique notification ID.
     */
    public int getNewNotificationId() {
        int notificationId = sNotificationId.getAndIncrement();

        // Unlikely in the sample, but the int will overflow if used enough so we skip the summary
        // ID. Most apps will prefer a more deterministic way of identifying an ID such as hashing
        // the content of the notification.
        if (notificationId == NOTIFICATION_GROUP_SUMMARY_ID) {
            notificationId = sNotificationId.getAndIncrement();
        }
        return notificationId;
    }

    /**
     * is image push
     *
     * @param extras
     * @return
     */
    private boolean isImagePush(Bundle extras) {
        try {
            if (!PhoneState.isNotificationNewStyle()) {
                throw new Exception("wrong os version");
            }
            String notiImg = extras.getString(KEY_NOTI_IMG);
            CLog.i("notiImg:" + notiImg);
            if (notiImg == null || "".equals(notiImg)) {
                throw new Exception("no image type");
            }
            return true;
        } catch (Exception e) {
            CLog.e("isImagePush:" + e.getMessage());
            return false;
        }
    }

    @TargetApi(Build.VERSION_CODES.O)
    private String createNotiChannel(Context context, NotificationManager notificationManager, String strNotiChannel)
    {
        NotificationChannel notiChannel = notificationManager.getNotificationChannel(strNotiChannel);
        boolean isShowBadge = false;
        boolean isEnableVibe;
        boolean isGroup = false;
        boolean isShowBadgeOnChannel;
        boolean isEnableVibeOnChannel;
        boolean isGroupOnChannel;
        boolean isPlaySoundOnChannel;
        boolean isVibrationPatternSet;
        boolean isVibrationPatternChanged = false;

        try
        {
            if(IPMSConsts.FLAG_Y.equals((String) context.getPackageManager().getApplicationInfo(context.getPackageName(),
                    PackageManager.GET_META_DATA).metaData.get(IPMSConsts.META_DATA_NOTI_O_BADGE)))
            {
                isShowBadge = true;
            }
            else
            {
                isShowBadge = false;
            }
        }
        catch (PackageManager.NameNotFoundException e)
        {
            CLog.e(e.getMessage());
        }
        if(IPMSConsts.FLAG_Y.equals(mPrefs.getString(PREF_VIBE_FLAG)))
        {
            isEnableVibe = true;
        }
        else
        {
            isEnableVibe = false;
        }
        try
        {
            CLog.d("AppSetting isShowBadge "+ context.getPackageManager().getApplicationInfo(context.getPackageName(),
                    PackageManager.GET_META_DATA).metaData.get(IPMSConsts.META_DATA_NOTI_O_BADGE)+
                    " isPlaySound "+mPrefs.getString(PREF_RING_FLAG)+
                    " isEnableVibe "+mPrefs.getString(PREF_VIBE_FLAG)+
                    " isVibrationPatternSet "+mPrefs.getString(IPMSConsts.PREF_VIBRATION_PATTERN));
        }
        catch (PackageManager.NameNotFoundException e)
        {
            CLog.e(e.getMessage());
        }
        if(mPrefs.getString(IPMSConsts.PREF_VIBRATION_PATTERN).length() > 0)
        {
            isVibrationPatternSet = true;
        }
        else
        {
            isVibrationPatternSet = false;
        }
        if (notiChannel == null)
        {    //if notichannel is not initialized
            CLog.d("notification initialized");
            notiChannel = new NotificationChannel(strNotiChannel, PMSUtil.getApplicationName(context), NotificationManager.IMPORTANCE_HIGH);
            notiChannel.setShowBadge(isShowBadge);
            notiChannel.enableVibration(isEnableVibe);
            notiChannel.setLightColor(Notification.FLAG_SHOW_LIGHTS);
            notiChannel.setImportance(NotificationManager.IMPORTANCE_HIGH);
            notiChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            if (isEnableVibe)
            {
                if(isVibrationPatternSet)
                {
                    String vibrationPattern = mPrefs.getString(IPMSConsts.PREF_VIBRATION_PATTERN);
                    String[] vibrationPatternSplits = vibrationPattern.split(",");
                    long[] vibrationPatternLong = new long[vibrationPatternSplits.length];
                    for(int i=0; i<vibrationPatternSplits.length; i++)
                    {
                        vibrationPatternLong[i] = Long.valueOf(vibrationPatternSplits[i]);
                    }
                    notiChannel.setVibrationPattern(vibrationPatternLong);
                }
                else
                {
                    notiChannel.setVibrationPattern(new long[]{1000,1000});
                }
            }

            notiChannel.setSound(null, null);
            notificationManager.createNotificationChannel(notiChannel);
            if (isGroup)
            {
                NotificationChannelGroup notificationChannelGroup = new NotificationChannelGroup(NOTIFICATION_GROUP, NOTIFICATION_GROUP);
                notificationManager.createNotificationChannelGroup(notificationChannelGroup);
                notiChannel.setGroup(strNotiChannel);
            }
            return strNotiChannel;
        }
        else
        {
            CLog.d("notification is exist");
            if (notiChannel.canShowBadge())
            {
                isShowBadgeOnChannel = true;
            } else
            {
                isShowBadgeOnChannel = false;
            }
            if (notiChannel.getSound() != null)
            {
                isPlaySoundOnChannel = true;
            } else
            {
                isPlaySoundOnChannel = false;
            }
            if (notiChannel.shouldVibrate())
            {
                isEnableVibeOnChannel = true;
            } else
            {
                isEnableVibeOnChannel = false;
            }
            if (notificationManager.getNotificationChannelGroups().size() > 0)
            {
                isGroupOnChannel = true;
            } else
            {
                isGroupOnChannel = false;
            }
            if(isVibrationPatternSet)
            {
                String vibrationPattern = mPrefs.getString(IPMSConsts.PREF_VIBRATION_PATTERN);
                String[] vibrationPatternSplits = vibrationPattern.split(",");
                long[] vibrationPatternLong = new long[vibrationPatternSplits.length];
                for (int i = 0; i < vibrationPatternSplits.length; i++)
                {
                    vibrationPatternLong[i] = Long.valueOf(vibrationPatternSplits[i]);
                }
                if(notiChannel.getVibrationPattern()!=null)
                {
                    if(vibrationPatternLong.length == notiChannel.getVibrationPattern().length)
                    {
                        for(int i = 0; i < vibrationPatternLong.length; i++)
                        {
                            if(vibrationPatternLong[i] != notiChannel.getVibrationPattern()[i])
                            {
                                isVibrationPatternChanged = true;
                            }
                        }
                    }
                    else
                    {
                        isVibrationPatternChanged = true;
                    }
                }
                else
                {
                    isVibrationPatternChanged = true;
                }
            }
            String isShowBadgeOnChannelString = isShowBadgeOnChannel ? "Y" : "N";
            String isPlaySoundOnChannelString = isPlaySoundOnChannel ? "Y" : "N";
            String isEnableVibeOnChannelString = isEnableVibeOnChannel ? "Y" : "N";
            String isGroupOnChannelString = isGroupOnChannel ? "Y" : "N";
            String isVibrationPatternChangeString = isVibrationPatternChanged ? "Y":"N";
            CLog.d("ChannelSetting isShowBadge " + isShowBadgeOnChannelString +
                    " isPlaySound " + isPlaySoundOnChannelString +
                    " isEnableVibe " + isEnableVibeOnChannelString +
                    " isGroupOnChannel " + isGroupOnChannelString +
                    " isVibrationPatternChange " + isVibrationPatternChangeString);

            //if notichannel is exist -> check setting from channel with app setting
            if ((isShowBadge != isShowBadgeOnChannel) || (isPlaySoundOnChannel) || (isEnableVibe != isEnableVibeOnChannel) || (isGroup != isGroupOnChannel) || isVibrationPatternChanged)
            {
                CLog.d("notification setting is not matched");
                notificationManager.deleteNotificationChannel(strNotiChannel);
                if (isGroupOnChannel)
                {
                    notificationManager.deleteNotificationChannelGroup(NOTIFICATION_GROUP);
                }
                //is not matched
                int currentChannelInteger = 0;
                try
                {
                    currentChannelInteger = Integer.parseInt(strNotiChannel);
                }
                catch (Exception e)
                {
                    CLog.d("currentChannel parsing " + e.getMessage());
                }
                String newChannelString = ++currentChannelInteger + "";
                CLog.d("newchannelString : " + newChannelString);
                NotificationChannel newChannel = new NotificationChannel(newChannelString, PMSUtil.getApplicationName(context), NotificationManager.IMPORTANCE_HIGH);
                newChannel.setShowBadge(isShowBadge);
                newChannel.enableVibration(isEnableVibe);
                newChannel.setLightColor(Notification.FLAG_SHOW_LIGHTS);
                newChannel.setImportance(NotificationManager.IMPORTANCE_HIGH);
                newChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
                if (isEnableVibe)
                {
                    if(isVibrationPatternSet)
                    {
                        String vibrationPattern = mPrefs.getString(IPMSConsts.PREF_VIBRATION_PATTERN);
                        String[] vibrationPatternSplits = vibrationPattern.split(",");
                        long[] vibrationPatternLong = new long[vibrationPatternSplits.length];
                        for (int i = 0; i < vibrationPatternSplits.length; i++)
                        {
                            vibrationPatternLong[i] = Long.valueOf(vibrationPatternSplits[i]);
                        }
                        newChannel.setVibrationPattern(vibrationPatternLong);
                    }
                    else
                    {
                        newChannel.setVibrationPattern(new long[]{1000,1000});
                    }
                }

                newChannel.setSound(null,null);
                notificationManager.createNotificationChannel(newChannel);
                if (isGroup)
                {
                    NotificationChannelGroup newChannelGroup = new NotificationChannelGroup(NOTIFICATION_GROUP, NOTIFICATION_GROUP);
                    notificationManager.createNotificationChannelGroup(newChannelGroup);
                    newChannel.setGroup(newChannelString);
                }

                DataKeyUtil.setDBKey(context, DB_NOTI_CHANNEL_ID, newChannelString);
                return newChannelString;
            } else
            {
                //is matched
                return strNotiChannel;
            }
        }
    }
}
