package com.pms.sdk.push.mqtt;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;

import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DataKeyUtil;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.push.PushReceiver;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;
import org.eclipse.paho.client.mqttv3.logging.MLog;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.io.ByteArrayInputStream;
import java.security.NoSuchAlgorithmException;

import static com.pms.sdk.IPMSConsts.FLAG_Y;

/**
 * MQTTBinder
 * @author haewon
 */
public class MQTTBinder
{
    public static final String ACTION_RECEIVED_MSG = "org.mosquitto.android.mqtt.MSGRECVD";
    public static final String KEY_MSG = "org.mosquitto.android.mqtt.MSG";

    private static int HANDLER_CLOSE_ID;
    private static String KEY_HANDLER_CLOSE_ID = "KEY_HANDLER_CLOSE_ID";

    private static final String MQTT_PROTOCOL_SSL = "ssl";
    private static final String DB_SSL_SIGN_KEY = "ssl_sign_key";
    private static final String DB_SSL_SIGN_PASS = "ssl_sign_pass";

    private static final int MQTT_RETRY_CNT = 1;

    private static MQTTBinder instance;
    private static ConnectInfo child;

    public Context ctx;

    private static String clientId;
    private static String topic;

    private Prefs pref;
    private String url;

    private MQTTBinder(Context ctx)
    {
        this.ctx = ctx;
    }

    public static MQTTBinder newInstance(Context ctx)
    {
        if (instance == null)
        {
            instance = new MQTTBinder(ctx);
        }

        return instance;
    }

    public static MQTTBinder getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException();
        }

        return instance;
    }

    public ConnectInfo getChild()
    {
        return child;
    }

    /**
     * MQTTBinder connection information
     * @return
     */
    public ConnectInfo withInfo(String clientId, String protocol, String host, int port)
    {
        this.clientId = clientId;

        url = protocol + "://" + host + ":" + port;

        topic = PMSUtil.getApplicationKey(ctx);
        pref = new Prefs(ctx);
        if (child == null)
        {
            child = new ConnectInfo(ctx, protocol, url);

            child.addOption();
            child.prepareClient(url);
        }
        else
        {
            child.beforeStopAfterRun();
        }

        HANDLER_CLOSE_ID = pref.getInt(KEY_HANDLER_CLOSE_ID, 1000) + 1;
        if (HANDLER_CLOSE_ID > 1000000)
        {
            HANDLER_CLOSE_ID = 1000;
        }

        pref.putInt(KEY_HANDLER_CLOSE_ID, HANDLER_CLOSE_ID);

        return child;
    }

    public static class ConnectInfo
    {
        private Context context;
        private MqttAsyncClient client;
        private MqttConnectOptions option;

        private IMQTTServiceCallback callback;
        private String url;

        public int currentCnt;
        public String protocol;

        private ConnectInfo(Context context, String protocol, String url)
        {
            this.context = context;
            this.protocol = protocol;
            this.url = url;
        }

        private void prepareClient(String url)
        {
            try
            {
                client = new MqttAsyncClient(url, clientId , new MemoryPersistence());
            }
            catch (MqttException e)
            {
                CLog.e(MLog.getMessageByResponseCode(e.toString()));
            }
        }

        private void addOption()
        {
            option = new MqttConnectOptions();
            if (MQTT_PROTOCOL_SSL.equals(protocol))
            {
                try
                {
                    String userName = DataKeyUtil.getDBKey(context, DB_SSL_SIGN_KEY);
                    String password = DataKeyUtil.getDBKey(context, DB_SSL_SIGN_PASS);
                    option.setSocketFactory(SelfSignedSocketFactory.getSSLSocketFactory(new ByteArrayInputStream(userName.getBytes()), password));
                }
                catch (NoSuchAlgorithmException e)
                {
                    CLog.e("[[ ConnectInfo ]] NoSuchAlgorithmException: " + e.getMessage());
                }
                catch (Exception e)
                {
                    CLog.e("[[ ConnectInfo ]] " + e.getMessage());
                    return;
                }
            }

            option.setCleanSession(true);
            option.setConnectionTimeout(30);
            option.setUserName(clientId);
            option.setPassword(topic.toCharArray());
        }

        private synchronized final void start()
        {
            try
            {
                if (client == null)
                {
                    addOption();
                    prepareClient(url);
                }

                client.setCallback(mqttCallback);
                client.connect(option, clientId, actionCallback);
            }
            catch (MqttSecurityException e)
            {
                CLog.e("[[ start ]] " + e.getMessage());
            }
            catch (MqttException e)
            {
                CLog.e("[[ start ]] " + MLog.getMessageByResponseCode(e.toString()));
            }
        }

        public synchronized final void start(IMQTTServiceCallback callback)
        {
            if (callback != null)
            {
                this.callback = callback;
            }

            start();
        }

        public void closeToAfterMillisecond(long millisecond)
        {
            closeMQTTHandler.sendEmptyMessageDelayed(MQTTBinder.HANDLER_CLOSE_ID, millisecond);
        }

        public void cancelListener()
        {
            closeMQTTHandler.removeMessages(MQTTBinder.HANDLER_CLOSE_ID);
        }

        private synchronized void retryConnect()
        {
            String currentUri = client.getCurrentServerURI();

            try
            {
                client.disconnectForcibly(0, 0);
                client.close(true);
                client = null;
            }
            catch (Exception e) { }

//            if (currentCnt == MQTT_RETRY_CNT)
//            {
//                StringBuilder sb = new StringBuilder(currentUri.substring(0, currentUri.lastIndexOf(":") + 1));
//                sb.append(1883);
//                prepareClient(sb.toString());
//            }
//            else
//            {
//                prepareClient(currentUri);
//            }

            if (TextUtils.isEmpty(currentUri))
            {
                return;
            }

            if(PMSUtil.getPrivateFlag(context).equals(FLAG_Y) && PMSUtil.getMQTTFlag(context).equals(FLAG_Y))
            {
                prepareClient(currentUri);
                start();
            }
            else
            {
                CLog.i("retry is cancelled because of flag is off");
            }
        }

        private void close()
        {
            if (client != null)
            {
                boolean isConnect = client.isConnected();
                CLog.i("[[ close ]] isConnect: " + isConnect);
                if (isConnect)
                {
                    try
                    {
                        client.disconnectForcibly(0, 0);
                        client.close(true);
                        client = null;
                    }
                    catch (MqttException e)
                    {
                        CLog.w("[[ close ]] "+MLog.getMessageByResponseCode(e.toString()));
                    }
                    catch (Exception e) { }
                }
            }
            else
            {
                CLog.w("[[ disconnect ]] client is null");
            }

            if (callback != null)
            {
                callback.onFinish();
            }
        }

        private void beforeStopAfterRun()
        {
            if (client != null)
            {
                boolean isConnect = client.isConnected();
                CLog.i("[[ beforeStopAfterRun ]] isConnect: " + isConnect);
                if (isConnect)
                {
                    try
                    {
                        client.disconnect(0, null, new IMqttActionListener()
                        {
                            @Override
                            public void onSuccess(IMqttToken iMqttToken)
                            {
                                CLog.d("[[ beforeStopAfterRun, onSuccess ]] ");
                                start();
                            }

                            @Override
                            public void onFailure(IMqttToken iMqttToken, Throwable throwable)
                            {
                                CLog.d("[[ beforeStopAfterRun, onFailure ]] " + throwable);
                            }
                        });
                    }
                    catch (MqttException e)
                    {
                        CLog.w("[[ beforeStopAfterRun ]] "+MLog.getMessageByResponseCode(e.toString()));
                    }
                    catch (Exception e) { }
                }
            }
        }

        private IMqttActionListener actionCallback = new IMqttActionListener()
        {
            @Override
            public void onSuccess(IMqttToken iMqttToken)
            {
                try
                {
                    currentCnt = 0;
                    client.subscribe(topic, 1);

                    if (callback != null)
                    {
                        callback.onConnect(child);
                    }
                }
                catch (MqttException e)
                {
                    CLog.e("[[ onSuccess ]] "+MLog.getMessageByResponseCode(e.toString()));
                }
            }

            @Override
            public void onFailure(IMqttToken iMqttToken, Throwable throwable)
            {
                if(throwable!=null)
                {
                    CLog.w("[[ onFailure ]] currentUri:" + client.getCurrentServerURI()+" "+MLog.getMessageByResponseCode(throwable.toString()));

                }
                else
                {
                    CLog.w("[[ onFailure ]] currentUri:" + client.getCurrentServerURI());

                }
                try
                {
                    Thread.sleep(2000);
                }
                catch (Exception e) { }
                CLog.d("[[ onFailure ]] currentCnt(" + currentCnt + ")");
                if (MQTT_RETRY_CNT > currentCnt)
                {
                    currentCnt++;
                    try
                    {
                        retryConnect();
                    }
                    catch (Exception e)
                    {
                        CLog.w("[[ onFailure ]] retryConnect: " + e.getMessage());
                    }
                }
                else
                {
                    try
                    {
                        currentCnt = 0;
                        close();
                    }
                    catch (Exception e)
                    {
                        CLog.w(e.getMessage());
                    }
                }
            }
        };

        private MqttCallback mqttCallback = new MqttCallback()
        {
            @Override
            public void connectionLost(Throwable throwable)
            {
                if(throwable != null)
                {
                    CLog.w("[[ connectionLost ]]" + MLog.getMessageByResponseCode(throwable.toString()));
                }
                else
                {
                    CLog.w("[[ connectionLost ]]");
                }
            }

            @Override
            public void messageArrived(String s, MqttMessage mqttMessage)
            {
                if (mqttMessage == null)
                {
                    CLog.w("[[ messageArrived ]] message is empty");
                    return;
                }

                if (mqttMessage.getPayload() == null)
                {
                    CLog.w("[[ messageArrived ]] message payload is empty");
                    return;
                }

                String payload = new String(mqttMessage.getPayload());
                CLog.i("[[ messageArrived ]] messageArrived!!!! messageId : " + mqttMessage.getId() + " payload : " + payload);

                Intent broadcastIntent = new Intent(context, PushReceiver.class);
                broadcastIntent.setAction(ACTION_RECEIVED_MSG);
                broadcastIntent.putExtra(KEY_MSG, payload);
                context.sendBroadcast(broadcastIntent);
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {   }
        };

        private Handler closeMQTTHandler = new Handler()
        {
            @Override
            public void handleMessage(Message msg)
            {
                if (msg.what == HANDLER_CLOSE_ID)
                {
                    removeMessages(HANDLER_CLOSE_ID);
                    close();
                }
            }
        };
    }

    /**
     * MQTTBinder service running action (interface)callback
     * @author haewon
     */
    public interface IMQTTServiceCallback
    {
        void onConnect(ConnectInfo child);
        void onFinish();
    }
}
