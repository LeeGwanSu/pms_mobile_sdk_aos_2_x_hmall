package com.pms.sdk.push.mqtt;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.text.TextUtils;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.PMSUtil;

import java.net.URI;

/**
 * Private PUSH Restart Receiver
 * @author haewon
 *
 */
public class RestartReceiver extends BroadcastReceiver implements IPMSConsts
{
	@Override
	public synchronized void onReceive (final Context context, Intent intent)
	{
		if(intent != null)
		{
			String action = intent.getAction();
			MQTTScheduler scheduler = MQTTScheduler.getInstance();

			if(!TextUtils.isEmpty(action))
			{
				boolean isUsingDozeMode = false;
				int device = Build.VERSION.SDK_INT;
				CLog.d("Device build version: " + device + ", Release version: " + Build.VERSION.RELEASE);
				// Doze Mode 사용여부 체크
				if (device >= Build.VERSION_CODES.M && device >= 23)
				{
					isUsingDozeMode = true;
				}

				if(action.equals(ACTION_FORCE_START))
				{
					CLog.d("[ RestartReceiver ] action: ACTION_FORCE_START");
					if (isUsingDozeMode)
					{
						try
						{
							URI serverUri = new URI(PMSUtil.getMQTTServerUrl(context));

							String serverProtocol = serverUri.getScheme();
							String serverHost = serverUri.getHost();
							int serverPort = serverUri.getPort();

							String clientId = PMSUtil.getAppUserId(context);
							if(!TextUtils.isEmpty(clientId))
							{
								MQTTBinder.newInstance(context).withInfo(clientId, serverProtocol, serverHost, serverPort).start(new MQTTBinder.IMQTTServiceCallback()
								{
									// connection complete
									@Override
									public void onConnect(MQTTBinder.ConnectInfo child)
									{
										child.closeToAfterMillisecond(MQTTScheduler.DEFAULT_KEEP_ALIVE_TIME);
									}

									// connection close
									@Override
									public void onFinish()
									{
										CLog.d("[ Binder finish ]");
									}
								});
							}
						}
						catch (Exception e)
						{
							CLog.e("Receiver error(mqtt) " + e.getMessage());
						}
					}
				}
				else if (action.equals(ACTION_START) || action.equals(ACTION_RESTART) || action.equals(ACTION_BOOT_COMPLETED))
				{
					if (!isUsingDozeMode)
					{
						try
						{
							// alarm register
							scheduler.setSchedule(context);

							// mqtt service
							Intent serviceIntent = new Intent(context, MQTTService.class);
							serviceIntent.putExtra(MQTTService.INTENT_ACTION, action);
							context.startService(serviceIntent);
						}
						catch (Exception e)
						{
							CLog.e("[ RestartReceiver ] service is not started! " + e.getMessage());
						}
					}
				}
				else
				{
					CLog.w("[ RestartReceiver ] not support action: " + action);
				}
			}
			else
			{
				CLog.w("[ RestartReceiver ] intent action is empty");
			}
		}
		else
		{
			CLog.w("[ RestartReceiver ] intent is null");
		}
	}
}
